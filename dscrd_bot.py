# Work with Python 3.6
import discord
import sys

try :
    TOKEN = sys.argv[1]
except:
    print("Require token as first argument")
    sys.exit()

client = discord.Client()

@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    # if message.author == client.user:
        # return

    if message.content.startswith('!register'):
        channel = message.channel
        ip = message.content.lstrip("!register").strip()
        print("[ONLINE] ",ip)
        await channel.send("Arigato !")
    
    if message.content.startswith('!unregister'):
        channel = message.channel
        print("[OFFLINE]",ip)
        await channel.send("Nani !?")

@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print('-'*20)
    
    # # Send message on ready
    # hard_coded_channel = await client.fetch_channel("675056130535194636")
    # await hard_coded_channel.send("I'm ready!")


client.run(TOKEN)